<?php 
include 'conn.php';
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="admin/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="index.css">
    <title>Naghma Store</title>
  </head>
  <body id="page-up">
  <!-- header -->
  <nav class="navbar navbar-expand-lg navbar-light bg-transparant fixed-top">
    <div class="container">
        <a class="navbar-brand font-weight-bold text-white" href="#">NAGHMA STORE</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
                <a class="nav-link js-scroll-trigger text-white" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger text-white" href="produk.php">Produk</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger text-white" href="order.php">Order</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger text-white" href="checkout.php">Check Out</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger text-white" href="about.php">About</a>
            </li>
            <?php if (isset($_SESSION['masuk'])): ?>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger text-white" href="riwayat.php">Riwayat</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-warning text-black" href="logout.php">LOGOUT</a>
            </li>
            <!-- jika belum ada session -->
            <?php else: ?>
            <li class="nav-item">
                <a class="btn btn-warning text-black" href="login.php">LOGIN | DAFTAR</a>
            </li>
            <?php endif ?>
            </ul>
        </div>
    </div>
  </nav>


  <!-- jumbotron -->
  <div class="jumbotron">
    <div class="container">
        <h1 class="display-4">CARI KEBUTUHAN MUSIKMU DI <br> 
        <span class="font-weight-bold">NAGHMA 
        <span class="text">STORE</span></span></h1>
        <hr class="my-4">
        <p class="lead">cepat murah dan terpercaya</p>
        <a class="btn btn-warning btn-lg" href="produk.php" role="button">SELENGKAPNYA</a>
    </div>
  </div>





    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>