<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="admin/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Naghma Store</title>
  </head>
  <body id="page-up">
<!-- header -->
  <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
    <div class="container">
        <a class="navbar-brand font-weight-bold" href="#">NAGHMA STORE</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="index.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="produk.php">Produk</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="order.php">Order</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="checkout.php">Check Out</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="about.php">About</a>
            </li>
            <!-- jika sudah ada session -->
            <?php if (isset($_SESSION['masuk'])): ?>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="riwayat.php">Riwayat</a>
            </li>
            <li class="nav-item">
                <a class="btn btn-warning" href="logout.php">LOGOUT</a>
            </li>
            <!-- jika belum ada session -->
            <?php else: ?>
            <li class="nav-item">
                <a class="btn btn-warning" href="login.php">LOGIN | DAFTAR</a>
            </li>
            <?php endif ?>
        </ul>
        </div>
    </div>
  </nav>