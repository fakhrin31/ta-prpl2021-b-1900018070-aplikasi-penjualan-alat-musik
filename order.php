<?php 
include 'conn.php';
// jika tidak ada session 
if (!isset($_SESSION['masuk'])) {
    echo "<script>alert('Silahkan LOGIN terlebih dahulu');</script>";
echo "<script>location='login.php'</script>";
}

if (empty($_SESSION['cart']) OR !isset($_SESSION['cart'])) {
    echo "<script>alert('Keranjang kosong, silahkan belanja terlebih dahulu');</script>";
    echo "<script>location='produk.php'</script>";
}
 ?>

  <?php 

  include 'header.php';
   ?>
   
  <section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Keranjang Pesanan</h1>
     </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-12">
            <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">No.</th>
                        <th scope="col">Foto</th>
                        <th scope="col" class="text-center">Harga</th>
                        <th scope="col">Produk</th>
                        <th scope="col">Jumlah</th>
                        <th scope="col" class="text-center">Sub Total</th>
                        <th scope="col" class="text-center">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $nomer=1; ?>
                    <?php foreach ($_SESSION['cart'] as $id => $jumlah): ?>
                        <!-- menampilkan produk
                        sesuai id_produk -->
                    <?php $data = $koneksi->query("SELECT * FROM produk WHERE id_produk='$id'"); 
                    $detail = $data->fetch_assoc(); ?>
                    <!-- <?php 
                    // echo "<pre>";
                    // print_r($pecah);
                    // echo "</pre>";
                    ?> -->
                    <tr>
                        <td><?php echo $nomer; ?></td>
                        <td><img src="admin/img_produk/<?php echo $detail['foto_produk']; ?>" width="100px"/></td>
                        <td class="text-center">Rp <?php echo number_format($detail['harga_produk']) ?></td>
                        <td><?php echo $detail['nama_produk']; ?></td>
                        <td><?php echo $jumlah ?></td>
                        <td class="text-center">Rp <?php echo number_format($jumlah*$detail['harga_produk']) ?></td>
                        <td class="text-center"><a href="hapus_order.php?&id=<?php echo $detail['id_produk']; ?>" class="btn btn-danger btn-sm">Hapus</a></td>
                    </tr>
                    <?php $nomer++; ?>
                    <?php endforeach ?>
                </tbody>                
            </table>
            <div class="form-group row mt-5">
                <div class="col-md-6">
                    <!-- back to home -->
                    <a name="backBtn" id="backBtn" class="btn btn-dark btn-block" href="produk.php" role="button">Lanjut Belanja</a>
                </div>
                <div class="col-md-6">
                    <!-- input button to submit form. Please check href attribute -->
                     <a name="backBtn" id="backBtn" class="btn btn-warning btn-block" href="checkout.php" role="button">Check Out</a>
                </div>
            </div>
        </div>
    </div>
</div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>