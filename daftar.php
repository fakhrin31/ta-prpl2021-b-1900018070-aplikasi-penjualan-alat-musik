<?php 
include 'conn.php';
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="admin/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="login.css">
    <title>Naghma Store</title>
  </head>
  <body>
  <div class="container">
    <div class="row content">
      <div class="col-md-6 mb-3">
        <img src="img/kyrie.png" alt="" class="img-responsive img-fluid">
      </div>
        <div class="col-md-6">
          <h4 class="text-center">DAFTAR</h4>
          <hr>
          <p class="text-center">Sudah punya akun? <a href="login.php">Masuk</a></p>
          <form method="POST">
            <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Jenis kelamin:</label><br>
              <label><input type="radio" name="jk" value="Laki-Laki" /> Laki-laki</label>
              <label><input type="radio" name="jk" value="Perempuan" /> Perempuan</label>
            </div>
            <div class="form-group">
              <label>No Telepon/HP</label>
              <input type="text" name="telp" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Email</label>
              <input type="email" name="email" class="form-control" placeholder="example@gmail.com" required>
            </div>
            <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" name="password" class="form-control" required>
            </div>
            <div class="form-group">
              <label>Alamat</label>
              <textarea class="form-control" name="alamat" required></textarea>
            </div>
            <div class="form-group">
              <button class="btn btn-warning btn-block" name="daftar">Daftar</button>
              <a name="backBtn" id="backBtn" class="btn btn-dark btn-block" href="index.php" role="button">Kembali</a>
            </div>
          </form>
          <?php 
          //mendaftarkan customer
          if (isset($_POST['daftar'])) {
            //mengambil data dari form
            $nama = $_POST['nama'];
            $jk = $_POST['jk'];
            $telp = $_POST['telp'];
            $email = $_POST['email'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $alamat = $_POST['alamat'];
            //cek sudah terpakai
            $cek = $koneksi->query("SELECT * FROM customer WHERE email_customer = '$email' OR username_customer = '$username'");

            $ambil = $cek->num_rows;
            if ($ambil==1) {
              echo "<script>alert('Username atau Email telah digunakan');</script>";
              echo "<script>location='daftar.php'</script>";
            }
            else{
              //menyimpan di database
              $koneksi->query("INSERT INTO customer (nama_customer, jk_customer, username_customer, email_customer, password_customer, telp_customer, alamat_customer) 
                VALUES ('$nama', '$jk', '$username', '$email', '$password', '$telp', '$alamat')");

              echo "<script>alert('Pendaftaran Sukses, silahkan login');</script>";
              echo "<script>location='login.php'</script>";
            }
            

          }
           ?>
      </div>
    </div>
  </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>