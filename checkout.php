<?php 
include 'conn.php';

// jika tidak ada session
if (!isset($_SESSION['masuk'])) {
    echo "<script>alert('Silahkan LOGIN terlebih dahulu');</script>";
echo "<script>location='login.php'</script>";
}
 ?>
    <?php 
    include 'header.php';
    ?>
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Checkout</h1>
         </div>
    </section>
  <!-- produk -->
  <div class="container">
       <div class="row"><!-- 
        <pre>
            <?php //print_r($_SESSION['masuk']); ?>
        </pre> -->
            <div class="col-12">
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Foto</th>
                            <th scope="col" class="text-center">Harga</th>
                            <th scope="col">Produk</th>
                            <th scope="col">Jumlah</th>
                            <th scope="col" class="text-center">Sub Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $nomer=1; ?>
                        <?php $total_belanja = 0; ?>
                        <?php if (@$_SESSION['cart']) {
                        ?>
                        <?php foreach ($_SESSION['cart'] as $id => $jumlah): ?>
                            <!-- menampilkan produk
                            sesuai id_produk -->
                        <?php $data = $koneksi->query("SELECT * FROM produk WHERE id_produk='$id'"); 
                        $cek = $data->fetch_assoc(); ?>
                        <!-- <?php 
                        // echo "<pre>";
                        // print_r($pecah);
                        // echo "</pre>";
                        ?> -->
                        <tr>
                            <td><?php echo $nomer; ?></td>
                            <td><img src="admin/img_produk/<?php echo $cek['foto_produk']; ?>" width="100px"/></td>
                            <td class="text-center">Rp <?php echo number_format($cek['harga_produk']) ?></td>
                            <td><?php echo $cek['nama_produk']; ?></td>
                            <td><?php echo $jumlah ?></td>
                            <td class="text-center">Rp <?php echo number_format($sub = $jumlah*$cek['harga_produk']) ?></td>
                        </tr>
                        <?php $nomer++; ?>
                        <?php $total_belanja += $sub ?>
                        <?php endforeach ?>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="5" class="text-right">Total Belanja</th>
                            <th class="text-center">Rp <?php echo number_format($total_belanja) ?></th>
                        </tr>
                    </tfoot>                
                </table>
                <form method="POST">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" readonly value="<?php echo($_SESSION['masuk']['nama_customer']); ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" readonly value="<?php echo($_SESSION['masuk']['telp_customer']); ?>">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" name="id_ongkir">
                                    <option>Piling Ongkir</option>
                                    <?php 
                                    $data = $koneksi->query("SELECT * FROM ongkir");
                                    while ($ongkir = $data->fetch_assoc()) {
                                     ?>
                                    <option value="<?php echo $ongkir['id_ongkir']; ?>"><?php echo $ongkir['nama_kota']; ?> (Rp <?php echo number_format($ongkir['tarif_ongkir']) ?>)</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label><strong>Alamat Lengkap Pengiriman</strong></label>
                        <textarea class="form-control" name="alamat" placeholder="Masukkan Alamat secara lengkap beserta kode POS"></textarea>
                    </div>
                    <button class="btn btn-success float-right" name="confirm">Confirm</button>
                </form>
                <?php 
                if (isset($_POST['confirm'])) {
                     $id = $_SESSION['masuk']['id_customer'];
                     $id_ongkir = $_POST["id_ongkir"];
                     $tgl_order = date("Y-m-d");
                     $alamat = $_POST['alamat'];

                     $data = $koneksi->query("SELECT * FROM ongkir WHERE id_ongkir='$id_ongkir'");
                     $ambil = $data->fetch_assoc();
                     $tarif = $ambil['tarif_ongkir'];
                     $kota = $ambil['nama_kota'];

                     $total = $total_belanja + $tarif;

                     // menyimpan data ke tabel order
                     $koneksi->query("INSERT INTO `order`(id_customer, tgl_order, total, id_ongkir, kota, tarif, alamat) VALUES('$id','$tgl_order','$total','$id_ongkir','$kota', '$tarif', '$alamat')"); 

                     //mendapatkan id order terlebih dahulu
                     $id_order = $koneksi->insert_id;

                     foreach ($_SESSION['cart'] as $id_produk => $jumlah) {
                        //mendapatkan data produk ke tabel order_detail
                    $dapat = $koneksi->query("SELECT * FROM  produk WHERE id_produk='$id_produk'");
                    $produk = $dapat->fetch_assoc();

                    $nama = $produk['nama_produk'];
                    $harga = $produk['harga_produk'];
                    $berat = $produk['berat_produk'];

                    
                    $sub_harga = $produk['harga_produk'] * $jumlah;
                    $sub_berat = $produk['berat_produk'] * $jumlah; 

                         //mrnyimpan data ke tabel order_detail
                     $koneksi->query("INSERT INTO order_detail(id_order, id_produk, jumlah_order, nama, harga, berat, sub_harga, sub_berat) VALUES('$id_order', '$id_produk', '$jumlah', '$nama', '$harga', '$berat', '$sub_harga', '$sub_berat')");

                      //update stok jika dibeli
                     $koneksi->query("UPDATE produk SET stok_produk = stok_produk-$jumlah WHERE id_produk = '$id_produk'");
                     }
                     //mengkosongkan keranjang order
                     unset($_SESSION['cart']);

                     //dipindahkan ke halaman nota pembelian
                     echo "<script>alert('Pembelian Berhasil');</script>";
                     echo "<script>location='nota.php?id=$id_order';</script>";

                 } ?>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>