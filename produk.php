<?php 
include 'conn.php';
 ?>

  <?php 

  include 'header.php';
   ?>

   <section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Produk</h1>
     </div>
    </section>
  <!-- produk --> 
    <div class="container">
        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-md-4">
                <form class="form-inline my-2 my-lg-0 float-right" method="GET" action="pencarian.php">
                    <div class="form-group">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
                        <button class="btn btn-light my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
                    </div>
                </form>
            </div>
        </div>
        <br><br>
        <div class="row">
        <?php 
        $data = $koneksi->query("SELECT * FROM produk");
        while ($produk = $data->fetch_assoc()) {
         ?>
            <div class="col-md-3">
                <div class="card">
                    <img src="admin/img_produk/<?php echo $produk['foto_produk']; ?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="caption"><?php echo $produk['nama_produk'] ?></h5>
                        <p>RP <?php echo number_format($produk['harga_produk']) ?></p>
                        <?php if ($produk['stok_produk']==0): ?>
                            <button class="btn btn-danger">SOLD OUT</button>
                        <?php else: ?>
                            <a href="detail.php?id=<?php echo $produk['id_produk'] ?>" class="btn btn-dark">Detail</a>
                            <a href="pesan.php?id=<?php echo $produk['id_produk'] ?>" class="btn btn-warning">Pesan</a>
                        <?php endif ?>
                    </div>
                </div>  
            </div>
        <?php } ?>
        </div>
    </div>




    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>