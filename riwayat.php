<?php 
include 'conn.php';
 ?>

<?php 
if (!isset($_SESSION['masuk'])) {
    echo "<script>alert('Silahkan LOGIN terlebih dahulu');</script>";
echo "<script>location='login.php'</script>";
}
?>

  <?php 

  include 'header.php';
   ?>

  <!-- produk -->
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Riwayat Pembelian</h1>
         </div>
    </section>
  	<div class="container">
  		<div class="row">

  			<div class="col-12">
          <?php if(isset($_GET['success']) && $_GET['success']==0) {?>
          <div class="alert alert-danger" role="alert">
            <?php echo @$_GET['msg']?>
          </div>
        <?php }?>
                <table class="table table-striped">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">No.</th>
                            <th scope="col">Tanggal Order</th>
                            <th scope="col">Status</th>
                            <th scope="col">Total</th>
                            <th scope="col">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    	<?php 
                    	$nomer =1;
                    	// mendapat id_customer
                    	$id = $_SESSION['masuk']['id_customer'];
                    	$data = $koneksi->query("SELECT * FROM `order` WHERE id_customer='$id'");
                    	while ($detail = $data->fetch_assoc()) {
               
                    	 ?>
                        <tr>
                            <td><?php echo $nomer; ?></td>
                            <td><?php echo $detail['tgl_order']; ?></td>
                            <td ><?php echo $detail['status_order']; ?>
                                <?php 
                                if (!empty($detail['no_resi'])): ?> <br>
                                  <?php echo $detail['no_resi']; ?>
                                  <?php endif ?>
                            </td>
                            <td>Rp <?php echo number_format($detail['total']); ?></td>
                            <td>
                            	<a href="nota.php?id=<?php echo $detail['id_order'] ?>" class="btn btn-warning">Nota</a>
                              <?php if ($detail['status_order']=="Pending"): ?>
                                <a href="pembayaran.php?id=<?php echo $detail['id_order'] ?>" class="btn btn-dark">Pembayaran</a>
                              <?php else: ?>
                                <a href="lihatpemb.php?id=<?php echo $detail['id_order'] ?>" class="btn btn-info">Pembayaran</a>
                              <?php endif ?>
                        	</td>
                        </tr>
                        <?php $nomer++; ?>
                    	<?php } ?>
                    </tbody>
                </table>
            </div>
  		</div>
  	</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
    $('.alert').hide('slow');  
    },3000)
    
  })
</script>
</body>
</html>