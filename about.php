<?php 
include 'conn.php';
 ?>
  <?php 

  include 'header.php';
   ?>
  <!-- produk -->
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Tentang Kami</h1>
         </div>
    </section>

  <div class="container">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h2>Detail</h2>
                    </div>
                    <div class="card-body">
                        Musik merupakan suatu kesenian yang bisa dijadikan sebagai sarana pengutaraan sebuah perasaan disemua kalangan mulai dari kaum muda hingga kaum tua. Dimana apabila ingin membuat suatu irama musik yang merdu dan enak untuk dinikmati maka dibutuhkan sebuah alat yang berkualitas. Maka kami hadirkanlah website untuk mendukung para musisi dalam mendukung terciptanya sebuah karya yang membanggakan
                    </div>
                    <div class="card-header">
                        <h2>Hubungi Kami</h2>
                    </div>
                    <div class="card-body">
                        <div class="col-md-4">
                            <div class="form-group">
                                <i class="fa fa-phone"> +62 822-xxxx-xxxx</i>
                            </div>                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <i class="fa fa-envelope"> naghma.store@company.com</i> 
                            </div>                            
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <i class="fa fa-map-marker"> Jalan mana yang kamu tuju</i>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>