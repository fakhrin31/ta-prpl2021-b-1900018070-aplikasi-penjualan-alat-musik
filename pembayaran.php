<?php 
include 'conn.php';
if(!isset($_GET['id']) || !$_GET['id']){
	header('location:riwayat.php?success=0&msg=ID Order salah');
}


// print_r($_SESSION);
$query = "SELECT * FROM `order` WHERE id_order = ".$_GET['id']." AND id_customer = ".$_SESSION['masuk']['id_customer'];
$total = $koneksi->query($query);

if(!$total->num_rows){
	header('location:riwayat.php?success=0&msg=ID Order salah');	
}

$detail = $total->fetch_assoc();
 ?>
  <?php 

  include 'header.php';
   ?>
  <!-- produk --> 
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Konfirmasi Pembelian</h1>
         </div>
    </section>

    <div class="container">
    	<div class="row">
    		<div class="col-12">
    			<form method="POST" enctype="multipart/form-data">
    				<div class="form-group">
						<label>ID Order</label>
						<?php echo $_GET['id']; ?>
					</div>
					<div class="form-group">
						<label>Tanggal Transfer</label>
						<input type="date" class="form-control" name="tanggal" required>
					</div>
					<div class="form-group">
						<label>Bank Tujuan</label>
						<?php 
						$bank = $koneksi->query("SELECT * FROM bank");
						
						// echo "<pre>";print_r($data);
						// exit;
						 ?>
						<select name="bank_tujuan" class="form-control" required>
							<option>Pilih Bank</option>
							<?php while ($data = $bank->fetch_assoc()) {
								echo "<option value='".$data['id_bank']."'>".$data['nama_bank']." - ".$data['no_rek']."</option>";
							} ?>
						</select>
					</div>
					<label>Total</label>
					<div class="input-group mb-3">
						<div class="input-group-prepend">
					    	<span class="input-group-text" id="basic-addon1">Rp</span>
					  	</div>
					  	<input type="text" name="total" class="form-control" aria-label="Username" aria-describedby="basic-addon1" value="<?php echo number_format($detail['total'],0,',','.'); ?>" required>
					</div>
					<div class="form-group">
						<label>Bukti Transfer</label>
						<input type="file" class="form-control" name="foto_bukti" required>
					</div>
					<div class="form-group row mt-5">
				        <div class="col-md-6">
				        	<!-- back to home -->
				        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block" href="riwayat.php" role="button">Kembali</a>
				        </div>
				        <div class="col-md-6">
				        	<!-- input button to submit form. Please check href attribute -->
				        	<button class="btn btn-warning btn-block" name="konfirmasi">Konfirmasi</button>
				        </div>
				    </div>
				</form>
    		</div>
    	</div>
    </div>
    <?php 
		if (isset($_POST['konfirmasi'])) {
			echo "<pre>";
			print_r($_FILES);
			$newName = time().rand(100,999).'.jpg';
			// exit;
			$nama = $newName;
			$lokasi = $_FILES['foto_bukti']['tmp_name'];
			move_uploaded_file($lokasi, "bukti_transfer/".$nama);
			// move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)
			$koneksi->query("INSERT INTO pembayaran (id_order, tgl_transfer, id_bank, total_transfer, bukti_transfer) 
							VALUES('".$_GET['id']."', 
									'".$_POST['tanggal']."', 
									'".$_POST['bank_tujuan']."', 
									'".str_replace('.', '', $_POST['total'])."', 
									'".$newName."')
								");
			$koneksi->query("UPDATE `order` SET status_order = 'Verifikasi Pembayaran' WHERE id_order='".$_GET["id"]."'"); 
			echo "<script>alert('Terimakasih');</script>";
			echo "<script>location='riwayat.php';</script>";
		}
     ?>
</body>
</html>