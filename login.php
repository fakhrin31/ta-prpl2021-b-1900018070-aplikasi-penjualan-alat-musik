<?php 
  include 'conn.php';
 ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="admin/assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="admin/assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="admin/assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="admin/assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="login.css">
    <title>Naghma Store</title>
  </head>
  <body>
  <div class="container">
    <div class="row content">
      <div class="col-md-6 mb-3">
        <img src="img/kyrie.png" alt="image" class="img-fluid">
      </div>
        <div class="col-md-6">
          <h4 class="text-center">Masuk</h4>
          <hr>
          <p class="text-center">Belum punya akun? <a href="daftar.php"> Daftar</a></p>
          <form method="POST">
          <div class="form-group">
            <label for="username">Username</label>
            <input type="text" name="username" class="form-control">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" name="password" class="form-control">
          </div>
          <div class="form-group">
            <button class="btn btn-warning btn-block" name="login">LOGIN</button>
            <a name="backBtn" id="backBtn" class="btn btn-dark btn-block" href="index.php" role="button">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php 
  // jika login ditekan
  if (isset($_POST['login'])) {
    $username = $_POST['username'];
    $password = $_POST['password'];
     //cek akun dengan query
    $data = $koneksi->query("SELECT * FROM customer WHERE username_customer='$username' AND password_customer = '$password'");

    //ambil akun
    $ambil = $data->num_rows; 
    $location = 'index.php';

    if(!$ambil){
       $data = $koneksi->query("SELECT * FROM admin WHERE username='$username' AND password = '$password'");
       $ambil = $data->num_rows;
       $location = 'admin/index.php';
    }
    //akun login
    if ($ambil==1) {
      //berhasil login
      $akun = $data->fetch_assoc();
      //menyimpan session login
      $_SESSION["masuk"] = $akun;
      echo "<script>alert('LOGIN SUKSES');</script>";
      echo "<script>location='$location'</script>";
    }
    else{
      //gagal login
      echo "<script>alert('LOGIN GAGAL');</script>";
    }
  }
  ?>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>