<?php 
include 'conn.php';
 ?>
 <?php 

	//mendapatkan id produk
	$id = $_GET["id"];
  ?>
 	<!-- header -->
  <?php 
  include 'header.php';
   ?>
  <section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Detail Produk</h1>
    </div>
  </section>
  <div class="container">
    <div class="row">
    <?php 
	//query data produk
	$data = $koneksi->query("SELECT * FROM produk WHERE id_produk = '$id'");
	$detail = $data->fetch_assoc();
	?>
      <div class="col-md-6">
        <img src="admin/img_produk/<?php echo $detail['foto_produk']; ?>" alt="" class="img-responsive img-fluid">
      </div>
      <div class="col-md-6">
        <h2><?php echo $detail['nama_produk']; ?></h2>
        <h4>RP <?php echo number_format($detail['harga_produk']); ?></h4>
        <p>Stok : <?php echo $detail['stok_produk']; ?></p>

        <form method="POST">
        	<div class="form-group">
        		<div class="input-group">
        			<input type="number" min="1" class="form-control" name="jumlah_order" max="<?php echo $detail['stok_produk']; ?>">
        		</div>
        	</div>
        	<p><?php echo $detail['deskripsi_produk']; ?></p>
        	<button class="btn btn-warning btn-block" name="pesan">Pesan</button>
            <a name="backBtn" id="backBtn" class="btn btn-dark btn-block" href="produk.php" role="button">Kembali</a>
        </form>
        <?php 
        //input produk ke order
        if (isset($_POST["pesan"])) {
        	//mengambil jumlah pesanan
        	$jumlah = $_POST['jumlah_order'];

        	//masuk ke order
        	$_SESSION['cart'][$id] = $jumlah;

        	 echo "<script>alert('Produk Berhasil Dipesan');</script>";
             echo "<script>location='order.php';</script>";
        }
         ?>
      </div>
    </div>
  </div>
 
 </body>
 </html>
