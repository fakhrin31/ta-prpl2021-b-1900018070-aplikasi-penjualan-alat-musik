<?php 
$kategori = array();
$data = $koneksi->query("SELECT * FROM kategori_produk");

while ($detail = $data->fetch_assoc()) {
	$kategori[]=$detail;
}
?>

<div class="card-header">
	<h2>Update Produk</h2>
</div>
<div class="card-body">
	<?php 
	$data = $koneksi->query("SELECT * FROM produk WHERE id_produk = '$_GET[id]'");
	$pecah = $data->fetch_assoc();
	 ?>

	 <form method="POST" enctype="multipart/form-data">
	 	 <div class="form-group">
			<label>Nama</label>
			<input type="text" class="form-control" name="nama_produk" value="<?php echo $pecah['nama_produk']; ?>">
		</div>
		<div class="form-group">
			<label>Kategori</label>
			<select class="form-control" name="kategori">
				<option value="">Pilih Kategori</option>
				<?php foreach ($kategori as $key => $val): ?>
					<option value="<?php echo $val['id_kategori']; ?>" <?php if ($pecah['id_kategori']==$val['id_kategori']) {
						echo "selected";
					} ?>>
					<?php echo $val['nama_kategori']; ?>
					</option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Berat(gr.)</label>
			<input type="number" class="form-control" name="berat_produk" value="<?php echo $pecah['berat_produk']; ?>">
		</div>
		<div class="form-group">
			<label>Stok</label>
			<input type="number" class="form-control" name="stok_produk" value="<?php echo $pecah['stok_produk']; ?>">
		</div>
		<div class="form-group">
			<label>Deskripsi</label>
			<textarea class="form-control" name="deskripsi_produk" rows="10"><?php echo $pecah['deskripsi_produk']; ?></textarea>
		</div>
		<div class="form-group">
			<label>Harga (Rp)</label>
			<input type="number" class="form-control" name="harga_produk" value="<?php echo $pecah['harga_produk']; ?>">
		</div>
		<div class="form-group">
			<img src="img_produk/<?php echo $pecah['foto_produk']; ?>" width = "350px">
		</div>
		<div class="form-group">
			<label>Ganti Foto</label>
			<input type="file" class="form-control" name="foto_produk">
		</div>
		<div class="form-group row mt-5">
	        <div class="col-md-6">
	        	<!-- back to home -->
	        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=produk" role="button">Kembali</a>
	        </div>
	        <div class="col-md-6">
	        	<!-- button to submit form. Please check href attribute -->
	        	<button class="btn btn-info btn-block btn-lg" name="update">Update</button>
	        </div>
	    </div>
	 </form>
	 <br>
</div>
<?php 
if (isset($_POST['update'])) {
	$nama = $_FILES['foto_produk']['name'];
	$lokasi = $_FILES['foto_produk']['tmp_name'];

	// lokasi foto dirubah
	
	if (!empty($lokasi)) {
		move_uploaded_file($lokasi, "img_produk/".$nama);
		// query update
		$koneksi->query("UPDATE produk SET id_kategori = '$_POST[kategori]', 'nama_produk = '$_POST[nama_produk]', berat_produk = '$_POST[berat_produk]', stok_produk = '$_POST[stok_produk]', deskripsi_produk = '$_POST[deskripsi_produk]' , harga_produk = '$_POST[harga_produk]', foto_produk = '$nama' WHERE id_produk = '$_GET[id]'"); 
	}
	else {
		$koneksi->query("UPDATE produk SET id_kategori = '$_POST[kategori]', nama_produk = '$_POST[nama_produk]', berat_produk = '$_POST[berat_produk]', stok_produk = '$_POST[stok_produk]', deskripsi_produk = '$_POST[deskripsi_produk]' , harga_produk = '$_POST[harga_produk]' WHERE id_produk = '$_GET[id]'"); 
	}
	echo "<script>alert('Produk Telah Diupdate');</script>";
	echo "<script>location='index.php?halaman=produk';</script>";
}
 ?>
