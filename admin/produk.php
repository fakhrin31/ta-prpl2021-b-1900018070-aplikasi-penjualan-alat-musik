<div class="card-header">
	<h2>Produk</h2>
</div>
<div class="card-body">
    <a href="index.php?halaman=tambah_produk" class="btn btn-success btn-sm"><i class="fa fa-fw fa-plus"></i> Tambah</a>
	<table class="table table-striped mt-3">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Produk</th>
				<th>Kategori</th>
				<th>Berat</th>
				<th>Stok</th>
				<th>Deskripsi</th>
				<th>Harga</th>
				<th>Foto</th>
				<th>Aksi</th>
			</tr>
		</thead>	
		<tbody>
			<?php $nomer=1; ?>
			<?php $data = $koneksi->query("SELECT * FROM produk JOIN kategori_produk 
			ON produk.id_kategori = kategori_produk.id_kategori");?>
			<?php while($pecah = $data->fetch_assoc()){?>
				<tr>
					<td><?php echo $nomer; ?></td>
					<td><?php echo $pecah['nama_produk']; ?></td>
					<td><?php echo $pecah['nama_kategori']; ?></td>
		            <td><?php echo $pecah['berat_produk']; ?></td>
		            <td><?php echo $pecah['stok_produk']; ?></td>
		            <td><?php echo $pecah['deskripsi_produk']; ?></td>
		            <td><?php echo $pecah['harga_produk']; ?></td>
		            <td><img src="img_produk/<?php echo $pecah['foto_produk']; ?>" width = "100px"></td>
		            <td>
		            	<a href="index.php?halaman=hapus_produk&id= <?php echo $pecah['id_produk']; ?>" class="btn btn-danger btn-sm">Hapus</a>
		            	<a href="index.php?halaman=update_produk&id= <?php echo $pecah['id_produk']; ?>" class="btn btn-warning btn-sm">Update</a>
		            </td>
		        </tr>
		    <?php $nomer++; ?>
		    <?php } ?>
		</tbody>
	</table>
</div>