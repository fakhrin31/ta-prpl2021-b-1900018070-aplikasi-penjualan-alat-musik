<div class="card-header">
	<h2>Tambah Bank</h2>
</div>
<div class="card-body">
	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label>Nama Bank</label>
			<input type="text" class="form-control" name="nama_bank">
		</div>
		<div class="form-group">
			<label>Nomor Rekening</label>
			<input type="text" class="form-control" name="no_rek">
		</div>
		<div class="form-group row mt-5">
	        <div class="col-md-6">
	        	<!-- back to home -->
	        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=bank" role="button">Kembali</a>
	        </div>
	        <div class="col-md-6">
	        	<!-- input button to submit form. Please check href attribute -->
	        	<button class="btn btn-info btn-block btn-lg" name="submit">Submit</button>
	        </div>
	    </div>
	</form>
</div>

<?php 
if (isset($_POST['submit'])) {
	$koneksi->query("INSERT INTO bank (nama_bank,no_rek) VALUES('$_POST[nama_bank]', '$_POST[no_rek]')"); 
	echo "<script>alert('Bank Telah Ditambahkan');</script>";
	echo "<script>location='index.php?halaman=bank';</script>";
}
 ?>
