<div class="card-header">
	<h2>Tambah Admin</h2>
</div>
<div class="card-body">
	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label>Nama</label>
			<input type="text" class="form-control" name="nama_admin">
		</div>
		<div class="form-group">
			<label>Username</label>
			<input type="text" class="form-control" name="username">
		</div>
		<div class="form-group">
			<label>Password</label>
			<input type="text" class="form-control" name="password">
		</div> 
		<div class="form-group">
			<label>Email</label>
			<input type="email" class="form-control" name="email_admin">
		</div>
		<div class="form-group row mt-5">
	        <div class="col-md-6">
	        	<!-- back to home -->
	        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=admin" role="button">Kembali</a>
	        </div>
	        <div class="col-md-6">
	        	<!-- input button to submit form. Please check href attribute -->
	        	<button class="btn btn-info btn-block btn-lg" name="submit">Submit</button>
	        </div>
	    </div>
	</form>
	<br>
</div>

<?php 
if (isset($_POST['submit'])) {
	$koneksi->query("INSERT INTO admin (username, password, nama_admin, email_admin) 
					VALUES('$_POST[username]', '$_POST[password]', '$_POST[nama_admin]', '$_POST[email_admin]')"); 
	echo "<script>alert('Admin Telah Ditambahkan');</script>";
	echo "<script>location='index.php?halaman=admin';</script>";
}
 ?>
