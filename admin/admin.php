<div class="card-header">
	<h2>Admin</h2>
</div>
<div class="card-body">
    <a href="index.php?halaman=tambah_admin" class="btn btn-success btn-sm"><i class="fa fa-fw fa-plus"></i> Tambah</a>
	<table class="table table-striped mt-3">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Admin</th>
				<th>Username</th>
				<th>Password</th>
				<th>E-mail</th>
				<th>Aksi</th>
			</tr>
		</thead>	
		<tbody>
			<?php $nomer=1; ?>
			<?php $data = $koneksi->query("SELECT * FROM admin");?>
			<?php while($pecah = $data->fetch_assoc()){?>
				<tr>
					<td><?php echo $nomer; ?></td>
					<td><?php echo $pecah['nama_admin']; ?></td>
					<td><?php echo $pecah['username']; ?></td>
					<td><?php echo $pecah['password']; ?></td>
					<td><?php echo $pecah['email_admin']; ?></td>
		            <td>
		            	<a href="index.php?halaman=hapus_admin&id= <?php echo $pecah['id_admin']; ?>" class="btn btn-danger btn-sm">Hapus</a>
		            	<a href="index.php?halaman=update_admin&id= <?php echo $pecah['id_admin']; ?>" class="btn btn-warning btn-sm">Update</a>
		            </td>
		        </tr>
		    <?php $nomer++; ?>
		    <?php } ?>
		</tbody>
	</table>