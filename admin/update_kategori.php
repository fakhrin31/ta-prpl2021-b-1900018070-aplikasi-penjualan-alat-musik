<div class="card-header">
	<h2>Update Kategori</h2>
</div>
<div class="card-body">
	<?php 
	$data = $koneksi->query("SELECT * FROM kategori_produk WHERE id_kategori='$_GET[id]'");
	$pecah = $data->fetch_assoc();
	 ?>

	 <form method="POST" enctype="multipart/form-data">
	 	 <div class="form-group">
			<label>Nama Kategori</label>
			<input type="text" class="form-control" name="nama_kategori" value="<?php echo $pecah['nama_kategori']; ?>">
		</div>
		<div class="form-group row mt-5">
	        <div class="col-md-6">
	        	<!-- back to home -->
	        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=kategori" role="button">Kembali</a>
	        </div>
	        <div class="col-md-6">
	        	<!-- input button to submit form. Please check href attribute -->
	        	<button class="btn btn-info btn-block btn-lg" name="update">Update</button>
	        </div>
	    </div>
	 </form>
	 <br>
</div>
<?php 
if (isset($_POST['update'])) {
	$koneksi->query("UPDATE kategori_produk SET nama_kategori = '$_POST[nama_kategori]' WHERE id_kategori = '$_GET[id]'"); 
	
	echo "<script>alert('Kategori Telah Diupdate');</script>";
	echo "<script>location='index.php?halaman=kategori';</script>";
	}
 ?>
