<div class="card-header">
	<h2>Kategori</h2>
</div>
<div class="card-body">
    <a href="index.php?halaman=tambah_kategori" class="btn btn-success btn-sm"><i class="fa fa-fw fa-plus"></i> Tambah</a>
	<table class="table table-striped mt-3">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Kategori</th>
				<th>Aksi</th>
			</tr>
		</thead>	
		<tbody>
			<?php $nomer=1; ?>
			<?php $data = $koneksi->query("SELECT * FROM kategori_produk");?>
			<?php while($pecah = $data->fetch_assoc()){?>
				<tr>
					<td><?php echo $nomer; ?></td>
					<td><?php echo $pecah['nama_kategori']; ?></td>
		            <td><a href="index.php?halaman=hapus_kategori&id= <?php echo $pecah['id_kategori']; ?>" class="btn btn-danger btn-sm">Hapus</a>
		            	<a href="index.php?halaman=update_kategori&id= <?php echo $pecah['id_kategori']; ?>" class="btn btn-warning btn-sm">Update</a>
		            </td>
		        </tr>
		    <?php $nomer++; ?>
		    <?php } ?>
		</tbody>
	</table>
</div>