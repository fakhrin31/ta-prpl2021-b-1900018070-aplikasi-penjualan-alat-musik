<?php
    session_start();
    include '../conn.php';
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link href="assets/vendor/fonts/circular-std/style.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/libs/css/style.css">
    <link rel="stylesheet" href="assets/vendor/fonts/fontawesome/css/fontawesome-all.css">
    <link rel="stylesheet" href="assets/vendor/charts/chartist-bundle/chartist.css">
    <link rel="stylesheet" href="assets/vendor/charts/morris-bundle/morris.css">
    <link rel="stylesheet" href="assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/vendor/charts/c3charts/c3.css">
    <link rel="stylesheet" href="assets/vendor/fonts/flag-icon-css/flag-icon.min.css">
    <title>Admin | NAGHMA STORE</title> 
</head> 
<body>
<!-- ============================================================== -->
<!-- main wrapper -->
<!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
    <!-- ============================================================== -->
    <!-- navbar -->
    <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top">
                <a class="navbar-brand text-danger" href="index.php">NAGHMA STORE</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                        <li class="nav-item">

                        </li>
                        <li class="nav-item dropdown nav-user">
                            <a class="nav-link nav-user-img" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Admin<i class="fas fa-angle-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" 
                            aria-labelledby="navbarDropdownMenuLink2">
                                <a class="dropdown-item" href="../logout.php">
                                    <i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                    aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                            <li class="nav-divider">
                                Menu
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="index.php" aria-expanded="false"
                                    data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-user-circle"></i>Dashboard
                                    
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false"
                                    data-target="#submenu-5" aria-controls="submenu-5"><i class="fas fa-fw fa-table"></i>Tables</a>
                                <div id="submenu-5" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.php?halaman=admin">Data Admin</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.php?halaman=customer">Data Customer</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.php?halaman=kategori">Data Kategori</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.php?halaman=produk">Data Produk</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.php?halaman=bank">Data Bank</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="index.php?halaman=order">Data Order</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" href="index.php?halaman=laporan" aria-expanded="false"
                                    data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-file"></i>Laporan
                                    
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../logout.php" aria-expanded="false"
                                    data-target="#submenu-1" aria-controls="submenu-1"><i class="fa fa-fw fa-sign-out-alt"></i>Log Out
                                    <span class="badge badge-success">6</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    <!-- ============================================================== -->
    <!-- end left sidebar -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- wrapper  -->
    <!-- ============================================================== -->
        <div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <?php 
                                if (isset($_GET['halaman'])) {
                                    if ($_GET['halaman']=="admin") {
                                        include 'admin.php';
                                    }
                                    else if ($_GET['halaman']=="customer") {
                                        include 'customer.php';
                                    }
                                    else if ($_GET['halaman']=="kategori") {
                                        include 'kategori.php';
                                    }
                                    else if ($_GET['halaman']=="produk") {
                                        include 'produk.php';
                                    }
                                    else if ($_GET['halaman']=="bank") {
                                        include 'bank.php';
                                    }
                                    else if ($_GET['halaman']=="order") {
                                        include 'order.php';
                                    }
                                    else if ($_GET['halaman']=="detail") {
                                        include 'detail.php';
                                    }
                                    else if ($_GET['halaman']=="tambah_produk") {
                                        include 'tambah_produk.php';
                                    }
                                    else if ($_GET['halaman']=="hapus_produk") {
                                        include 'hapus_produk.php';
                                    }
                                    else if ($_GET['halaman']=="update_produk") {
                                        include 'update_produk.php';
                                    }
                                    else if ($_GET['halaman']=="tambah_kategori") {
                                        include 'tambah_kategori.php';
                                    }
                                    else if ($_GET['halaman']=="hapus_kategori") {
                                        include 'hapus_kategori.php';
                                    }
                                    else if ($_GET['halaman']=="update_kategori") {
                                        include 'update_kategori.php';
                                    }
                                    else if ($_GET['halaman']=="dashboard") {
                                        include 'dashboard.php';
                                    }
                                    else if ($_GET['halaman']=="pembayaran") {
                                        include 'pembayaran.php';
                                    }
                                    else if ($_GET['halaman']=="laporan") {
                                        include 'laporan.php';
                                    }
                                    else if ($_GET['halaman']=="hapus_customer") {
                                        include 'hapus_customer.php';
                                    }
                                    else if ($_GET['halaman']=="tambah_bank") {
                                        include 'tambah_bank.php';
                                    }
                                    else if ($_GET['halaman']=="hapus_bank") {
                                        include 'hapus_bank.php';
                                    }
                                    else if ($_GET['halaman']=="update_bank") {
                                        include 'update_bank.php';
                                    }
                                    else if ($_GET['halaman']=="tambah_admin") {
                                        include 'tambah_admin.php';
                                    }
                                    else if ($_GET['halaman']=="hapus_admin") {
                                        include 'hapus_admin.php';
                                    }
                                    else if ($_GET['halaman']=="update_admin") {
                                        include 'update_admin.php';
                                    }
                                } 
                                else{
                                    include 'dashboard.php';
                                }   ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <a>Copyright © 2018 Concept. All rights reserved. Dashboard by Itengss</a>.
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="assets/vendor/jquery/jquery-3.3.1.min.js"></script>
    <!-- bootstap bundle js -->
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.js"></script>
    <!-- slimscroll js -->
    <script src="assets/vendor/slimscroll/jquery.slimscroll.js"></script>
    <!-- main js -->
    <script src="assets/libs/js/main-js.js"></script>
    <!-- chart chartist js -->
    <script src="assets/vendor/charts/chartist-bundle/chartist.min.js"></script>
    <!-- sparkline js -->
    <script src="assets/vendor/charts/sparkline/jquery.sparkline.js"></script>
    <!-- morris js -->
    <script src="assets/vendor/charts/morris-bundle/raphael.min.js"></script>
    <script src="assets/vendor/charts/morris-bundle/morris.js"></script>
    <!-- chart c3 js -->
    <script src="assets/vendor/charts/c3charts/c3.min.js"></script>
    <script src="assets/vendor/charts/c3charts/d3-5.4.0.min.js"></script>
    <script src="assets/vendor/charts/c3charts/C3chartjs.js"></script>
    <script src="assets/libs/js/dashboard-ecommerce.js"></script>
</body>

</html>