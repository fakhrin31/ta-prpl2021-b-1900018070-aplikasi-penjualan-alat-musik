<?php 
$kategori = array();
$data = $koneksi->query("SELECT * FROM kategori_produk");

while ($detail = $data->fetch_assoc()) {
	$kategori[]=$detail;
}
?>


<div class="card-header">
	<h2>Tambah Produk</h2>
</div>
<div class="card-body">
	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label>Nama</label>
			<input type="text" class="form-control" name="nama_produk">
		</div>
		<div class="form-group">
			<label>Kategori</label>
			<select class="form-control" name="kategori">
				<option value="">Pilih Kategori</option>
				<?php foreach ($kategori as $key => $val): ?>
					<option value="<?php echo $val['id_kategori']; ?>"><?php echo $val['nama_kategori']; ?></option>
				<?php endforeach ?>
			</select>
		</div>
		<div class="form-group">
			<label>Berat(gr.)</label>
			<input type="number" class="form-control" name="berat_produk">
		</div>
		<div class="form-group">
			<label>Stok</label>
			<input type="number" class="form-control" name="stok_produk">
		</div> 
		<div class="form-group">
			<label>Deskripsi</label>
			<textarea class="form-control" name="deskripsi_produk" rows="10"></textarea>
		</div>
		<div class="form-group">
			<label>Harga (Rp)</label>
			<input type="number" class="form-control" name="harga_produk">
		</div>
		<div class="form-group">
			<label>Foto</label>
			<input type="file" class="form-control" name="foto_produk">
		</div>
		<div class="form-group row mt-5">
	        <div class="col-md-6">
	        	<!-- back to home -->
	        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=produk" role="button">Kembali</a>
	        </div>
	        <div class="col-md-6">
	        	<!-- input button to submit form. Please check href attribute -->
	        	<button class="btn btn-info btn-block btn-lg" name="submit">Submit</button>
	        </div>
	    </div>
	</form>
	<br>
</div>

<?php 
if (isset($_POST['submit'])) {
	$nama = $_FILES['foto_produk']['name'];
	$lokasi = $_FILES['foto_produk']['tmp_name'];
	move_uploaded_file($lokasi, "img_produk/".$nama);
	$koneksi->query("INSERT INTO produk (id_kategori, nama_produk, berat_produk, stok_produk, deskripsi_produk, harga_produk, foto_produk) 
					VALUES('$_POST[kategori]', '$_POST[nama_produk]', '$_POST[berat_produk]', '$_POST[stok_produk]', '$_POST[deskripsi_produk]', '$_POST[harga_produk]','$nama')"); 
	echo "<script>alert('Produk Telah Ditambahkan');</script>";
	echo "<script>location='index.php?halaman=produk';</script>";
}
 ?>
