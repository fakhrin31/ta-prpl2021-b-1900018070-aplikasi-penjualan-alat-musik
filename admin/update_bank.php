<div class="card-header">
	<h2>Update Bank</h2>
</div>
<div class="card-body">
	<?php 
	$data = $koneksi->query("SELECT * FROM bank WHERE id_bank='$_GET[id]'");
	$pecah = $data->fetch_assoc();
	 ?>

	 <form method="POST" enctype="multipart/form-data">
	 	<div class="form-group">
			<label>Nama Bank</label>
			<input type="text" class="form-control" name="nama_bank" value="<?php echo $pecah['nama_bank']; ?>">
		</div>
		<div class="form-group">
			<label>Nomor Rekening</label>
			<input type="text" class="form-control" name="no_rek" value="<?php echo $pecah['no_rek']; ?>">
		</div>
		<div class="form-group row mt-5">
	        <div class="col-md-6">
	        	<!-- back to home -->
	        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=bank" role="button">Kembali</a>
	        </div>
	        <div class="col-md-6">
	        	<!-- input button to submit form. Please check href attribute -->
	        	<button class="btn btn-info btn-block btn-lg" name="update">Update</button>
	        </div>
	    </div>
	 </form>
	 <br>
</div>
<?php 
if (isset($_POST['update'])) {
	$koneksi->query("UPDATE bank SET nama_bank = '$_POST[nama_bank]', no_rek = '$_POST[no_rek]' WHERE id_bank = '$_GET[id]'"); 
	
	echo "<script>alert('Bank Telah Diupdate');</script>";
	echo "<script>location='index.php?halaman=bank';</script>";
	}
 ?>
