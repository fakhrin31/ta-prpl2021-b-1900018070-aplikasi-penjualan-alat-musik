<div class="card-header">
	<h2>Order</h2>
</div>
<div class="card-body">
	<table class="table table-striped mt-3">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Customer</th>
				<th>Tanggal</th>
				<th>Status Pembelian</th>
				<th>Total</th>
				<th>Aksi</th>
			</tr>
		</thead>	
		<tbody>
			<?php mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT) ?>
			<?php $nomer=1; ?>
			<?php $data = $koneksi->query("SELECT * FROM `order` JOIN customer ON `order`.id_customer=customer.id_customer");?>
			<?php while($pecah = $data->fetch_assoc()){?>
				<tr>
					<td><?php echo $nomer; ?></td>
					<td><?php echo $pecah['nama_customer']; ?></td>
					<td><?php echo $pecah['tgl_order']; ?></td>
					<td><?php echo $pecah['status_order']; ?></td>
					<td><?php echo $pecah['total']; ?></td>
		            <td><a href="index.php?halaman=detail&id=<?php echo $pecah['id_order'] ?>" class="btn btn-secondary btn-sm">Detail</a>
		            	<?php if ($pecah['status_order']!=="Pending"): ?>
		            		<a href="index.php?halaman=pembayaran&id=<?php echo $pecah['id_order'] ?>" class="btn btn-success btn-sm">Update</a>
		            	<?php endif ?>
		            </td>
		        </tr>
		    <?php $nomer++; ?>
		    <?php } ?>
		</tbody>
	</table>
</div>
