<?php 
$tampil = array();
$mulai = "---";
$akhir = "---";
if (isset($_POST['lihat'])) {
	$mulai = $_POST['mulai'];
	$akhir = $_POST['akhir'];
	$data = $koneksi->query("SELECT * FROM `order` JOIN customer ON `order`.id_customer = customer.id_customer 
		WHERE tgl_order BETWEEN '$mulai' AND '$akhir'");
	while ($detail = $data->fetch_assoc()) {
		$tampil[] = $detail;
	}
}
 ?>

<div class="card-header">
	<h2>Laporan Transaksi</h2>
</div>
<div class="card-body">
	<form method="POST">
		<div class="col-xl-5">
			<div class="form-group">
				<label>Mulai</label>
				<input type="date" name="mulai" class="form-control" value="<?php echo $mulai ?>">
			</div>
		</div>
		<div class="col-xl-5">
			<div class="form-group">
				<label>Akhir</label>
				<input type="date" name="akhir" class="form-control" value="<?php echo $akhir ?>">
			</div>
		</div>		
		<div class="col-xl-2">
			<div class="form-group">
				<label> </label>
				<button class="btn btn-primary" name="lihat">Lihat</button>
			</div>
		</div>
	</form>
	<h4 class="text-center">Periode</h4>
	<p class="text-center"><?php echo $mulai ?> s/d <?php echo $akhir ?></p>
	<table class="table table-striped mt-3">
		<tr>
			<th>No.</th>
			<th>Customer</th>
			<th>Tanggal</th>
			<th>Status</th>
			<th>Alamat</th>
			<th>Pembelian</th>
		</tr>
		<?php $total = 0; ?>
		<?php foreach ($tampil as $key => $val): ?>
		<?php $total += $val['total']; ?>
		<tr>
			<td><?php echo $key+1; ?></td>
			<td><?php echo $val['nama_customer'] ?></td>
			<td><?php echo $val['tgl_order'] ?></td>
			<td><?php echo $val['status_order'] ?></td>
			<td><?php echo $val['alamat'] ?></td>
			<td>Rp <?php echo number_format($val['total']) ?></td>
		</tr>
		<?php endforeach ?>
		<tr>
			<th colspan="5">Total</th>
			<th>Rp <?php echo number_format($total) ?></th>
		</tr>
	</table>

</div>
