<div class="card-header">
	<h2>Detail Pembayaran</h2>
</div>
<?php 
//mengambil id order
$id = $_GET['id'];

//detail pembayaran dari id order
$data = $koneksi->query("SELECT * FROM pembayaran JOIN `order`ON pembayaran.id_order = `order`.id_order WHERE pembayaran.id_order = '$id'");
$detail = $data->fetch_assoc();

$data2 = $koneksi->query("SELECT * FROM `order` JOIN customer ON `order`.id_customer=customer.id_customer WHERE `order`.id_order='$id'");
$pecah = $data2->fetch_assoc();
?>
<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<table class="table table-striped mt-3">
				<table class="table table-bordered">
					<tr>
						<th>Nama</th>
						<td><?php echo $pecah['nama_customer']; ?></td>
					</tr>
					<tr>
						<th>Email</th>
						<td><?php echo $pecah['email_customer']; ?></td>
					</tr>
					<tr>
						<th>No.Telpon</th>
						<td><?php echo $pecah['telp_customer']; ?></td>
					</tr>
					<tr>
						<th>Tanggal Order</th>
						<td><?php echo $pecah['tgl_order']; ?></td>
					</tr>
					<tr>
						<th>Tanggal Transfer</th>
						<td><?php echo $detail['tgl_transfer']; ?></td>
					</tr>
					<tr>
						<th>Total</th>
						<td>Rp <?php echo number_format($detail['total_transfer']); ?></td>
					</tr>
					<tr>
						<th>Alamat</th>
						<td><?php echo $pecah['alamat'] ?></td>
					</tr>
				</table>
			</table>
		</div>
	</div>
	<div class="col-md-6">
		<div class="card-body">
			<img src="../bukti_transfer/<?php echo $detail['bukti_transfer']; ?>" alt="" class="img-responsive img-fluid">
		</div>
	</div>
</div>

<div class="container">
	<form method="POST">
		<div class="form-group">
			<label>Nomor Resi Pengiriman</label>
			<?php 
			$data = $koneksi->query("SELECT * FROM `order` WHERE id_order='$id'");
			$detail = $data->fetch_assoc();
			if (!empty($detail['no_resi'])): ?>
            	<input type="text" name="resi" class="form-control" value="<?php echo $detail['no_resi']; ?>">
            <?php else: ?>
            	<input type="text" name="resi" class="form-control">
            <?php endif ?>
		</div>
		<div class="form-group">
			<label>Status Update</label>
			<select class="form-control" name="status">
				<option value="">Pilih Status</option>
				<option value="Sedang Dikemas">Sedang Dikemas</option>
				<option value="Paket sedang Dikirim">Paket sedang Dikirim</option>
				<option value="Paket Diterima">Paket Diterima</option>
				<option value="Batal">Batal</option>
			</select>
		</div>
		<button class="btn btn-success float-right" name="submit">Submit</button>
		<a href="index.php?halaman=order" class="btn btn-dark float-right">Kembali</a>
		<br><br><br>
	</form>

	<?php 
	if (isset($_POST['submit'])) {
		$resi = $_POST['resi'];
		$status = $_POST['status'];
		$koneksi->query("UPDATE `order` SET no_resi = '$resi', status_order = '$status' WHERE id_order = '$id'");
		echo "<script>alert('Berhasil diupdate');</script>";
		echo "<script>location='index.php?halaman=order';</script>";
	}
	 ?>
</div>