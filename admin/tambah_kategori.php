<div class="card-header">
	<h2>Tambah Kategori</h2>
</div>
<div class="card-body">
	<form method="POST" enctype="multipart/form-data">
		<div class="form-group">
			<label>Nama Kategori</label>
			<input type="text" class="form-control" name="nama_kategori">
		</div>
		<div class="form-group row mt-5">
	        <div class="col-md-6">
	        	<!-- back to home -->
	        	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=kategori" role="button">Kembali</a>
	        </div>
	        <div class="col-md-6">
	        	<!-- input button to submit form. Please check href attribute -->
	        	<button class="btn btn-info btn-block btn-lg" name="submit">Submit</button>
	        </div>
	    </div>
	</form>
</div>

<?php 
if (isset($_POST['submit'])) {
	$koneksi->query("INSERT INTO kategori_produk (nama_kategori) VALUES('$_POST[nama_kategori]')"); 
	echo "<script>alert('Kategori Telah Ditambahkan');</script>";
	echo "<script>location='index.php?halaman=kategori';</script>";
}
 ?>
