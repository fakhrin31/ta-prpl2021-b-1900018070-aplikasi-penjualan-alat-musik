<div class="card-header">
	<h2>Order</h2>
</div>
<div class="card-body">
	<div class="row">
		<?php $data = $koneksi->query("SELECT * FROM `order` JOIN customer ON `order`.id_customer=customer.id_customer
        WHERE `order`.id_order = '$_GET[id]'");?>
        <?php $detail = $data->fetch_assoc(); ?>
        <div class="col-md-6">
            <strong>No. Order : <?php echo $detail['id_order'] ?></strong>
            <p>
	            Nama : <?php echo $detail['nama_customer'] ?><br>
	            No.Telepon : <?php echo $detail['telp_customer']; ?> <br>
	            Email : <?php echo $detail['email_customer']; ?>
            </p>
            <p>
                Alamat Pengiriman : <?php echo $detail['alamat']; ?>
            </p>
        </div>
        <div class="col-md-6">
        <p>
        	Status Pembelian : <?php echo $detail['status_order']; ?><br>
            Tanggal Order : <?php echo $detail['tgl_order']; ?><br>
            Ongkir ke : <?php echo $detail['kota']; ?> <br>
            Rp <?php echo number_format($detail['tarif']); ?>
        </p>
        <p>
            <strong>Total Pembelian : Rp <?php echo number_format($detail['total']); ?></strong>
        </p>
        </div>
    </div>
	<table class="table table-striped mt-3">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Produk</th>
				<th>Harga</th>
				<th>Jumlah Order</th>
				<th>Sub Total</th>
			</tr>
		</thead>
		<tbody>
			<?php $nomer=1; ?>
			<?php $detail = $koneksi->query("SELECT * FROM order_detail JOIN produk ON order_detail.id_produk=produk.id_produk
				WHERE order_detail.id_order = '$_GET[id]'"); ?>
			<?php while ($pecah = $detail->fetch_assoc()){  ?>
			<tr>
				<td><?php echo $nomer ?></td>
				<td><?php echo $pecah['nama_produk']; ?></td>
				<td><?php echo $pecah['harga_produk']; ?></td>
				<td><?php echo $pecah['jumlah_order']; ?></td>
				<td><?php echo $pecah['harga_produk']*$pecah['jumlah_order']; ?></td>
			</tr>
			<?php $nomer++; ?>
		<?php } ?>
		</tbody>
	</table>
	<div class="form-group row mt-5">
	    <div class="col-md-6">
		<!-- back to home -->
	    	<a name="backBtn" id="backBtn" class="btn btn-dark btn-block btn-lg" href="index.php?halaman=order" role="button">Kembali</a>
		</div>
	</div>
</div>