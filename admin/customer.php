<div class="card-header">
	<h2>Customer</h2>
</div>
<div class="card-body">
	<table class="table table-striped mt-3">
		<thead>
			<tr>
				<th>No.</th>
				<th>Nama Customer</th>
				<th>Jenis Kelamin</th>
				<th>Username</th>
				<th>Password</th>
				<th>E-mail</th>
				<th>Telepon</th>
				<th>Aksi</th>
			</tr>
		</thead>	
		<tbody>
			<?php $nomer=1; ?>
			<?php $data = $koneksi->query("SELECT * FROM customer");?>
			<?php while($pecah = $data->fetch_assoc()){?>
				<tr>
					<td><?php echo $nomer; ?></th>
					<td><?php echo $pecah['nama_customer']; ?></td>
					<td><?php echo $pecah['jk_customer']; ?></td>
					<td><?php echo $pecah['username_customer']; ?></td>
					<td><?php echo $pecah['password_customer']; ?></td>
					<td><?php echo $pecah['email_customer']; ?></td>
					<td><?php echo $pecah['telp_customer']; ?></td>
		            <td>
		            	<a href="index.php?halaman=hapus_customer&id=<?php echo $pecah['id_customer']; ?>" class="btn btn-danger btn-sm">Hapus</a>
		            </td>
		        </tr>
		    <?php $nomer++; ?>
		    <?php } ?>
		</tbody>
	</table>
</div>