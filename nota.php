<?php 
include 'conn.php';
 ?>
  <?php 

  include 'header.php';
   ?>
  <!-- produk -->
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Nota Pembelian</h1>
         </div>
    </section>

  <div class="container">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h2>Detail Pembelian</h2>
                    </div>
                    <div class="card-body">
                        <?php $data = $koneksi->query("SELECT * FROM `order` JOIN customer ON `order`.id_customer=customer.id_customer
                            WHERE `order`.id_order = '$_GET[id]'");?>
                        <?php $detail = $data->fetch_assoc(); ?>
                        <!-- <pre>
                            <?php //print_r($detail) ?>
                        </pre> -->


                        <!-- proteksi pada nota pembelian -->
                        <?php 
                        //mendapatkan id_customer yang order
                        $id_ord = $detail['id_customer'];
                        //mendapatkan id_customer order
                        $id_masuk = $_SESSION['masuk']['id_customer'];

                        //verifikasi
                        if ($id_ord !== $id_masuk) {
                            echo "<script>alert('ID tidak cocok, mohon diperiksa kembali');</script>";
                            echo "<script>location='riwayat.php';</script>";
                         } 
                         ?>
                        

                        <div class="row">
                            <div class="col-md-6">
                                <strong>No. Order : <?php echo $detail['id_order'] ?></strong>
                                <p>
                                    Nama : <?php echo $detail['nama_customer'] ?><br>
                                    No.Telepon : <?php echo $detail['telp_customer']; ?> <br>
                                    Email : <?php echo $detail['email_customer']; ?>
                                </p>
                                <p>
                                    Alamat Pengiriman : <?php echo $detail['alamat']; ?>
                                </p>
                            </div>
                            <div class="col-md-6">
                                <p>
                                    Status Pembelian : <?php echo $detail['status_order']; ?><br>
                                    Tanggal Order : <?php echo $detail['tgl_order']; ?><br>
                                    Ongkir ke : <?php echo $detail['kota']; ?> <br>
                                    Rp <?php echo number_format($detail['tarif']); ?>
                                </p>
                                <p>
                                    <strong>Total Pembelian : Rp <?php echo number_format($detail['total']); ?></strong>
                                </p>
                            </div>
                        </div>
                        <table class="table table-striped mt-3">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Produk</th>
                                    <th>Harga</th>
                                    <th>Jumlah Order</th>
                                    <th>Sub Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $nomer=1; ?>
                                <?php $detail = $koneksi->query("SELECT * FROM order_detail
                                    WHERE id_order = '$_GET[id]'"); ?>
                                <?php while ($pecah = $detail->fetch_assoc()){  ?>
                                <tr>
                                    <td><?php echo $nomer ?></td>
                                    <td><?php echo $pecah['nama']; ?></td>
                                    <td><?php echo $pecah['harga']; ?></td>
                                    <td><?php echo $pecah['jumlah_order']; ?></td>
                                    <td><?php echo $pecah['sub_harga']; ?></td>
                                </tr>
                                <?php $nomer++; ?>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="form-group row mt-5">
                            <div class="col-md-6">
                                    <!-- back to home -->
                                <a name="backBtn" id="backBtn" class="btn btn-dark btn-block" href="riwayat.php" role="button">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
    -->
  </body>
</html>