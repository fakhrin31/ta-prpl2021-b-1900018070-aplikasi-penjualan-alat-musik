<?php 
include 'conn.php';
 ?>
<?php 
      //mengambil id order
      $id = $_GET['id'];

      //detail pembayaran dari id order
      $data = $koneksi->query("SELECT * FROM pembayaran JOIN `order`ON pembayaran.id_order = `order`.id_order WHERE pembayaran.id_order = '$id'");
      $detail = $data->fetch_assoc();

      $data2 = $koneksi->query("SELECT * FROM `order` JOIN customer ON `order`.id_customer=customer.id_customer WHERE `order`.id_order='$id'");
      $pecah = $data2->fetch_assoc();
?>


<!-- jika beda akun yang melihat pembayaran orang lain -->
<?php 
if ($_SESSION['masuk']['id_customer']!==$detail['id_customer']) {
  echo "<script>alert('ID anda salah');</script>";
  echo "<script>location='riwayat.php';</script>"; 
}
 ?>

  <?php 

  include 'header.php';
   ?>

  <!-- produk -->
    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Riwayat Pembelian</h1>
         </div>
    </section>
  	<div class="container">
      <div class="row">
        <div class="col-md-6">
          <table class="table table-striped mt-3">
            <table class="table table-bordered">
              <tr>
                <th>Nama</th>
                <td><?php echo $pecah['nama_customer']; ?></td>
              </tr>
              <tr>
                <th>Email</th>
                <td><?php echo $pecah['email_customer']; ?></td>
              </tr>
              <tr>
                <th>No.Telpon</th>
                <td><?php echo $pecah['telp_customer']; ?></td>
              </tr>
              <tr>
                <th>Tanggal Order</th>
                <td><?php echo $pecah['tgl_order']; ?></td>
              </tr>
              <tr>
                <th>Tanggal Transfer</th>
                <td><?php echo $detail['tgl_transfer']; ?></td>
              </tr>
              <tr>
                <th>Total</th>
                <td>Rp <?php echo number_format($detail['total_transfer']); ?></td>
              </tr>
              <tr>
                <th>Alamat</th>
                <td><?php echo $pecah['alamat'] ?></td>
              </tr>
            </table>
          </table>
        </div>
        <div class="col-md-6">
          <br><br>
            <img src="bukti_transfer/<?php echo $detail['bukti_transfer']; ?>" alt="" class="img-responsive img-fluid">
        </div>
        <div class="col-md-6">
          <a href="riwayat.php" class="btn btn-dark btn-block">Kembali</a>
        </div>
      </div>
  	</div>
</body>
</html>