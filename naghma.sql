-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 04, 2021 at 06:13 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naghma`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama_admin` varchar(50) NOT NULL,
  `email_admin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`, `nama_admin`, `email_admin`) VALUES
(1, 'admin', 'admin', 'Fakhri N', 'mfakhrin@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id_bank` int(11) NOT NULL,
  `nama_bank` varchar(20) NOT NULL,
  `no_rek` varchar(100) NOT NULL,
  `atas_nama` varchar(100) NOT NULL,
  `logo_bank` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id_bank`, `nama_bank`, `no_rek`, `atas_nama`, `logo_bank`) VALUES
(1, 'BNI', '834259324', 'Muhammad Fakhri Nashruddin', ''),
(2, 'BCA', '1276445234', 'Fakhri N', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `nama_customer` varchar(50) NOT NULL,
  `jk_customer` text NOT NULL,
  `username_customer` varchar(50) NOT NULL,
  `email_customer` varchar(50) NOT NULL,
  `password_customer` varchar(50) NOT NULL,
  `telp_customer` varchar(50) NOT NULL,
  `alamat_customer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `jk_customer`, `username_customer`, `email_customer`, `password_customer`, `telp_customer`, `alamat_customer`) VALUES
(1, 'Ghea Anisa', 'Perempuan', 'ghea', 'ghea@gmail.com', 'ghea', '089673524134', ''),
(3, 'Seto Adi', 'Laki-Laki', 'seto', 'seto@gmail.com', 'seto', '089742674213', 'UMY sebelah utara, Ringroad Selatan'),
(7, 'Pewe', 'Perempuan', 'pewe', 'pewe@gmail.com', 'pewe', '08186728324', 'Jl Imogiri Timur, Banguntapan, Bantul');

-- --------------------------------------------------------

--
-- Table structure for table `kategori_produk`
--

CREATE TABLE `kategori_produk` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kategori_produk`
--

INSERT INTO `kategori_produk` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Guitar'),
(2, 'Biola'),
(3, 'Keyboard'),
(5, 'Bass Guitar'),
(6, 'Drum and Accesories'),
(7, 'Microphone'),
(8, 'Sound');

-- --------------------------------------------------------

--
-- Table structure for table `ongkir`
--

CREATE TABLE `ongkir` (
  `id_ongkir` int(11) NOT NULL,
  `nama_kota` varchar(50) NOT NULL,
  `tarif_ongkir` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ongkir`
--

INSERT INTO `ongkir` (`id_ongkir`, `nama_kota`, `tarif_ongkir`) VALUES
(1, 'Jogja', 12000),
(2, 'Semarang', 20000);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id_order` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `tgl_order` date NOT NULL,
  `total` int(50) NOT NULL,
  `id_ongkir` int(11) NOT NULL,
  `kota` varchar(50) NOT NULL,
  `tarif` int(11) NOT NULL,
  `alamat` text NOT NULL,
  `status_order` varchar(50) NOT NULL DEFAULT 'Pending',
  `no_resi` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id_order`, `id_customer`, `tgl_order`, `total`, `id_ongkir`, `kota`, `tarif`, `alamat`, `status_order`, `no_resi`) VALUES
(1, 1, '2021-05-01', 1700000, 1, '', 0, '', 'Pending', ''),
(2, 1, '2021-05-14', 1475000, 1, '', 0, '', 'Pending', ''),
(15, 1, '2021-05-20', 11562000, 1, '', 0, '', 'Pending', ''),
(16, 1, '2021-05-20', 11562000, 1, '', 0, '', 'Pending', ''),
(17, 1, '2021-05-20', 2970000, 2, '', 0, '', 'Pending', ''),
(18, 1, '2021-05-20', 4662000, 1, '', 0, '', 'Pending', ''),
(19, 1, '2021-05-20', 5862000, 1, 'Jogja', 12000, 'Kasihan Bantul, DIY 55181', 'Sedang Dikemas', ''),
(21, 3, '2021-06-02', 22362000, 1, 'Jogja', 12000, 'Padokan Lor RT 03 Tirtonirmolo Kasihan Bantul 55181', 'Paket sedang Dikirim', 'J5632484ds'),
(22, 1, '2021-06-03', 1495000, 2, 'Semarang', 20000, 'dfsgrge', 'Pending', ''),
(23, 3, '2021-06-04', 3937000, 1, 'Jogja', 12000, 'Padokan Lor', 'Pending', ''),
(24, 3, '2021-06-06', 1712000, 1, 'Jogja', 12000, 'UMY Yogyakarta 55181', 'Pending', ''),
(25, 1, '2021-06-07', 16320000, 2, 'Semarang', 20000, 'Jalan Ronggolawe No.2', 'Verifikasi Pembayaran', ''),
(26, 6, '2021-06-14', 17462000, 1, 'Jogja', 12000, 'Jl Imogiri Timur, Banguntapan, Bantul 55192', 'Pending', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `id_detail` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `jumlah_order` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `sub_harga` int(11) NOT NULL,
  `sub_berat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`id_detail`, `id_order`, `id_produk`, `jumlah_order`, `nama`, `harga`, `berat`, `sub_harga`, `sub_berat`) VALUES
(1, 1, 1, 2, '0', 0, 0, 0, 0),
(2, 1, 2, 1, '0', 0, 0, 0, 0),
(3, 11, 2, 1, '0', 0, 0, 0, 0),
(4, 11, 11, 2, '0', 0, 0, 0, 0),
(5, 12, 2, 1, '0', 0, 0, 0, 0),
(6, 12, 11, 2, '0', 0, 0, 0, 0),
(7, 13, 2, 1, '0', 0, 0, 0, 0),
(8, 13, 11, 2, '0', 0, 0, 0, 0),
(9, 13, 9, 1, '0', 0, 0, 0, 0),
(10, 14, 8, 1, '0', 0, 0, 0, 0),
(11, 16, 1, 2, 'Biola Cervini Cremona', 1700000, 200, 3400000, 400),
(12, 16, 11, 1, 'Biola Cervini Cremona', 1700000, 200, 1700000, 200),
(13, 17, 2, 2, 'Yamaha Guitar FX-310 / FX310 / FX 310', 1475000, 700, 2950000, 1400),
(14, 18, 2, 2, 'Yamaha Guitar FX-310 / FX310 / FX 310', 1475000, 700, 2950000, 1400),
(15, 18, 1, 1, 'Biola Cervini Cremona', 1700000, 200, 1700000, 200),
(16, 19, 1, 2, 'Biola Cervini Cremona', 1700000, 200, 3400000, 400),
(17, 19, 10, 1, 'JBL Control 23T, Contractor Series Control 23T', 2450000, 4100, 2450000, 4100),
(18, 21, 10, 3, 'JBL Control 23T, Contractor Series Control 23T', 2450000, 4100, 7350000, 12300),
(19, 21, 12, 1, 'AKG C214 Stereo Set', 15000000, 3500, 15000000, 3500),
(20, 22, 2, 1, 'Yamaha Guitar FX-310 / FX310 / FX 310', 1475000, 700, 1475000, 700),
(21, 23, 2, 1, 'Yamaha Guitar FX-310 / FX310 / FX 310', 1475000, 700, 1475000, 700),
(22, 23, 10, 1, 'JBL Control 23T, Contractor Series Control 23T', 2450000, 4100, 2450000, 4100),
(23, 24, 1, 1, 'Biola Cervini Cremona', 1700000, 200, 1700000, 200),
(24, 25, 11, 2, 'G&L Tribute JB2 3Tone Sunburst', 8150000, 3000, 16300000, 6000),
(25, 26, 10, 1, 'JBL Control 23T, Contractor Series Control 23T', 2450000, 4100, 2450000, 4100),
(26, 26, 12, 1, 'AKG C214 Stereo Set', 15000000, 3500, 15000000, 3500);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran`
--

CREATE TABLE `pembayaran` (
  `id_pembayaran` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `tgl_transfer` date NOT NULL,
  `id_bank` int(11) NOT NULL,
  `total_transfer` int(11) NOT NULL,
  `bukti_transfer` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pembayaran`
--

INSERT INTO `pembayaran` (`id_pembayaran`, `id_order`, `tgl_transfer`, `id_bank`, `total_transfer`, `bukti_transfer`, `created_at`) VALUES
(11, 21, '2021-06-02', 2, 22362000, '1622819739800.jpg', '2021-06-04 15:15:39'),
(12, 25, '2021-06-08', 2, 16320000, '1623129443682.jpg', '2021-06-08 05:17:23'),
(13, 19, '2021-06-06', 1, 5862000, '1623136925315.jpg', '2021-06-08 07:22:05');

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `nama_produk` varchar(100) NOT NULL,
  `berat_produk` int(11) NOT NULL,
  `stok_produk` int(11) NOT NULL,
  `deskripsi_produk` text NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `foto_produk` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `id_kategori`, `nama_produk`, `berat_produk`, `stok_produk`, `deskripsi_produk`, `harga_produk`, `foto_produk`) VALUES
(1, 2, 'Biola Cervini Cremona', 200, 4, 'biola ukuran 4/4 merek cervini by Cremonaseri HV 200 original', 1700000, 'produk1.jpg'),
(2, 1, 'Yamaha Guitar FX-310 / FX310 / FX 310', 700, 3, 'FX-310 NATURAL merupakan gitar yang pas bagi pemula', 1475000, 'produk2.jpg'),
(8, 3, 'Dave Smith Evolver Keys-1', 2000, 0, 'Brand Dave Smith Instrument berat 2000gr', 25750000, 'produk3.jpg'),
(9, 6, 'Ahead M-RK Monsta Rock', 200, 5, 'Stik Drum Merk Ahead', 600000, 'produk4.jpg'),
(10, 8, 'JBL Control 23T, Contractor Series Control 23T', 4100, 2, 'The Control® 23T is the most compact of JBL Professional’s Control Contractor Series indoor/outdoor loudspeakers', 2450000, 'produk5.jpg'),
(11, 5, 'G&L Tribute JB2 3Tone Sunburst', 3000, 5, 'The JB-2’s classic Alnico V pickups have tone reminiscent of the best examples from the early ‘60s', 8150000, 'produk6.jpg'),
(12, 7, 'AKG C214 Stereo Set', 3500, 3, 'Professional large-diaphragm condenser microphone', 15000000, 'produk7.jpg'),
(13, 1, 'Guitar APX1200II', 1200, 7, 'Sebagai puncak dari seri APX, APX1200 memiliki bagian belakang dan samping berbahan solid rosewood, sistem pickup model-mikro SRT Yamaha dan tampilan mewah di antaranya binding berbahan mahogany, tuning bertombol hitam, dan inlay abalon/mother-of-pearl yang terinspirasi oleh SG3000 yang legendaris.', 19800000, 'produk8.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id_bank`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `ongkir`
--
ALTER TABLE `ongkir`
  ADD PRIMARY KEY (`id_ongkir`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`),
  ADD KEY `id_customer` (`id_customer`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `pembayaran`
--
ALTER TABLE `pembayaran`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id_bank` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kategori_produk`
--
ALTER TABLE `kategori_produk`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ongkir`
--
ALTER TABLE `ongkir`
  MODIFY `id_ongkir` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id_order` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `pembayaran`
--
ALTER TABLE `pembayaran`
  MODIFY `id_pembayaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
