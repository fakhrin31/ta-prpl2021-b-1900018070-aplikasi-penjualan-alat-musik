<?php 
include 'conn.php';

$search = $_GET['search'];

$pencarian = array();
$data = $koneksi->query("SELECT * FROM produk WHERE nama_produk LIKE '%$search%' 
						OR deskripsi_produk LIKE '%$search%'");

while ($hasil = $data->fetch_assoc()) {
	
	$pencarian[]=$hasil;
}
 ?>

 <?php 
 include 'header.php';
  ?>
  <section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">Hasil Pencarian</h1>
    </div>
  </section>
  <div class="container">
  	<h5>Pencarian : <?php echo $search ?></h5>
  	<?php if (empty($pencarian)): ?>
  		<div class="alert alert-info">Produk dengan key "<?php echo $search ?>" tidak ditemukan</div>
  	<?php endif ?>
  	<div class="row">
      <div class="col-md-8"></div>
      <div class="col-md-4">
        <form class="form-inline my-2 my-lg-0 float-right" method="GET" action="pencarian.php">
          <div class="form-group">
              <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="search">
              <button class="btn btn-light my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
          </div>
        </form>
      </div><br><br>
  		<?php foreach ($pencarian as $key => $val): ?>
  			<div class="col-md-3">
                <div class="card">
                    <img src="admin/img_produk/<?php echo $val['foto_produk']; ?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="caption"><?php echo $val['nama_produk'] ?></h5>
                        <p>RP <?php echo number_format($val['harga_produk']) ?></p>
                        <?php if ($val['stok_produk']==0): ?>
                            <button class="btn btn-danger">SOLD OUT</button>
                        <?php else: ?>
                            <a href="detail.php?id=<?php echo $val['id_produk'] ?>" class="btn btn-dark">Detail</a>
                            <a href="pesan.php?id=<?php echo $val['id_produk'] ?>" class="btn btn-warning">Pesan</a>
                        <?php endif ?>
                    </div>
                </div>  
            </div>
  		<?php endforeach ?>
  	</div>
  </div>